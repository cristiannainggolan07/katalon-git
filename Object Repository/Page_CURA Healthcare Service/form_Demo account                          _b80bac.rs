<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_Demo account                          _b80bac</name>
   <tag></tag>
   <elementGuidId>00c238ab-2e5e-45f4-836b-f6806b9c8983</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>form.form-horizontal</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@action='https://katalon-demo-cura.herokuapp.com/authenticate.php']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>23ad1dc8-f9ff-4971-a310-e00adbce6acd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-horizontal</value>
      <webElementGuid>326e96a8-3eb1-4c6c-b7c0-12363dbe87de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>https://katalon-demo-cura.herokuapp.com/authenticate.php</value>
      <webElementGuid>a260c8ce-c25b-4f54-b170-a172c9552c73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>727dea71-cc47-4035-883f-ffdf09aa6d39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                </value>
      <webElementGuid>a4857c80-89b4-409c-a3ec-856570215c7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-offset-3 col-sm-6&quot;]/form[@class=&quot;form-horizontal&quot;]</value>
      <webElementGuid>c2a5ba3e-f17e-4ecf-a5ee-76e1b6d2e43e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='https://katalon-demo-cura.herokuapp.com/authenticate.php']</value>
      <webElementGuid>7c9ebfb6-6345-4ffe-a6ce-4bbc312a0a4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='login']/div/div/div[2]/form</value>
      <webElementGuid>7ae953a8-5f1e-45a0-bccf-a182efdca52a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/following::form[1]</value>
      <webElementGuid>1baa3d2b-1f9b-4c87-8006-6219b937e4a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::form[1]</value>
      <webElementGuid>a40ce039-129c-4b0e-a55c-5255460d9077</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
      <webElementGuid>567937da-3bbd-4cd5-a0f4-efa4b6d11012</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[(text() = '
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                ' or . = '
                    
                        
                            Demo account
                            
                                
                                  
                                  
                                
                            
                        
                        
                            
                                
                                  
                                  
                                
                            
                        
                    
                    
                        Username
                        
                            
                        
                    
                    
                        Password
                        
                            
                        
                    
                    
                        
                            Login
                        
                    
                ')]</value>
      <webElementGuid>adf0eea8-36f3-4b45-9e49-4b38480770eb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
